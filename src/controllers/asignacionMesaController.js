const { obtenerAsignacion,actualizarAsignacion, insertarAsignacion} = require('../services/asignacionMesaService');

function obtenerAsignacionController(req, res) {
    obtenerAsignacion((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}

function actualizarAsignacionController(req, res) {
    const parametrosEntrada = req.body;
  
    actualizarAsignacion(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
}

function insertarAsignacionMesaController(req,res){
  const parametrosEntrada = req.body;

  insertarAsignacion(parametrosEntrada, (err, resultados) =>{
    if (err){
      console.error('Error al ejecutar procedimiento: ', err);
      return res.status(500).json({error: 'Error al ejecutar el procedimiento'});
    }
    res.status(200).json(resultados);
  })
}


module.exports = { obtenerAsignacionController, actualizarAsignacionController,insertarAsignacionMesaController};