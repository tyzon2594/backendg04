const { obtenerPiso, modificarPiso, insertarPiso } = require('../services/pisoService');

function obtenerPisosController(req, res) {
    obtenerPiso((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}

function insertarPisoController(req, res) {
    const parametrosEntrada = req.body;
  
    insertarPiso(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }


function modificarPisoController(req, res) {
    const parametrosEntrada = req.body;
  
    modificarPiso(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }

module.exports = { obtenerPisosController, modificarPisoController, insertarPisoController };
