const { obtenerClientes,actualizarCliente, insertarCliente } = require('../services/clienteService');

function obtenerClientesController(req, res) {
    obtenerClientes((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}

function actualizarClienteController(req, res) {
    const parametrosEntrada = req.body;
  
    actualizarCliente(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
}

function insertarClienteController(req,res){
  const parametrosEntrada = req.body;

  insertarCliente(parametrosEntrada, (err, resultados) =>{
    if (err){
      console.error('Error al ejecutar procedimiento: ', err);
      return res.status(500).json({error: 'Error al ejecutar el procedimiento'});
    }
    res.status(200).json(resultados);
  })
}

module.exports = { obtenerClientesController, actualizarClienteController,insertarClienteController};