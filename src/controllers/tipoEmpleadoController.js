// controllers/elementosController.js

const { obtenerTipoEmpleado,modificarTipoEmpleado,insertarTipoEmpleado } = require('../services/tipoEmpleadoService');

function obtenerTipoEmpleadoController(req, res) {
    obtenerTipoEmpleado((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}

function modificarTipoEmpleadoController(req, res) {
    const parametrosEntrada = req.body;
  
    modificarTipoEmpleado(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }

  function insertarTipoEmpleadoController(req, res) {
    const parametrosEntrada = req.body;
  
    insertarTipoEmpleado(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }

module.exports = { obtenerTipoEmpleadoController,modificarTipoEmpleadoController,insertarTipoEmpleadoController };
