// controllers/elementosController.js

const { obtenerPersona,modificarPersona,insertarPersona } = require('../services/personaService');

function obtenerPersonaController(req, res) {
    obtenerPersona((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}

function modificarPersonaController(req, res) {
    const parametrosEntrada = req.body;
  
    modificarPersona(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }

  function insertarPersonaController(req, res) {
    const parametrosEntrada = req.body;
  
    insertarPersona(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }

module.exports = { obtenerPersonaController,modificarPersonaController,insertarPersonaController };
