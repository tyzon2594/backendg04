const { obtenerMesa, modificarMesa, insertarMesa } = require('../services/mesaService');

function obtenerMesasController(req, res) {
    obtenerMesa((err, elementos) => {
    if (err) {
      return res.status(500).json({ error: 'Error al obtener los elementos' });
    }
    res.status(200).json(elementos);
  });
}

function insertarMesaController(req, res) {
    const parametrosEntrada = req.body;
  
    insertarMesa(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }


function modificarMesaController(req, res) {
    const parametrosEntrada = req.body;
  
    modificarMesa(parametrosEntrada, (err, resultados) => {
      if (err) {
        console.error('Error al ejecutar el procedimiento:', err);
        return res.status(500).json({ error: 'Error al ejecutar el procedimiento' });
      }
      res.status(200).json(resultados);
    });
  }

module.exports = { obtenerMesasController, modificarMesaController, insertarMesaController };