// controllers/proveedorController.js

const {
    obtenerProveedorPorID,
    actualizarProveedor,
    registrarProveedor,
    eliminarProveedor
} = require('../services/proveedorService');

function obtenerProveedorPorIDController(req, res) {
    const idProveedor = req.params.id;

    obtenerProveedorPorID(idProveedor, (err, proveedor) => {
        if (err) {
            console.error('Error al obtener el proveedor:', err);
            return res.status(500).json({ error: 'Error al obtener el proveedor' });
        }
        res.status(200).json(proveedor);
    });
}

function registrarProveedorController(req, res) {
    const { nombre_empresa, contacto_nombre, contacto_telefono, contacto_email, direccion, id_administrador } = req.body;

    registrarProveedor(nombre_empresa, contacto_nombre, contacto_telefono, contacto_email, direccion, id_administrador, (error, resultado) => {
        if (error) {
            console.error('Error al registrar el proveedor:', error);
            return res.status(500).json({ error: 'Error al registrar el proveedor' });
        }
        res.status(200).json(resultado);
    });
}

async function actualizarProveedorController(req, res) {
    const idProveedor = req.params.id;
    const { nombre_empresa, contacto_nombre, contacto_telefono, contacto_email, direccion, id_administrador } = req.body;

    try {
        // Llama a actualizarProveedor utilizando async/await
        const resultados = await actualizarProveedor(idProveedor, nombre_empresa, contacto_nombre, contacto_telefono, contacto_email, direccion, id_administrador);
        
        // Envía una respuesta con los resultados
        res.status(200).json({ message: 'Proveedor actualizado exitosamente', results: resultados });
    } catch (error) {
        console.error('Error al actualizar el proveedor:', error);
        res.status(500).json({ error: 'Error al actualizar el proveedor' });
    }
}
function eliminarProveedorController(req, res) {
    const idProveedor = req.params.id;

    eliminarProveedor(idProveedor, (error, resultado) => {
        if (error) {
            console.error('Error al eliminar el proveedor:', error);
            return res.status(500).json({ error: 'Error al eliminar el proveedor' });
        }
        res.status(200).json({ message: 'Proveedor eliminado exitosamente', results: resultado });
    });
}

module.exports = { obtenerProveedorPorIDController, registrarProveedorController, actualizarProveedorController, eliminarProveedorController };

