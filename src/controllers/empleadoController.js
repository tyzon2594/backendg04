const {
    obtenerEmpleadoPorID,
    actualizarEmpleado,
    registrarEmpleado,
    eliminarEmpleado
} = require('../services/empleadoService');

function obtenerEmpleadoPorIDController(req, res) {
    const idEmpleado = req.params.id;

    obtenerEmpleadoPorID(idEmpleado, (err, empleado) => {
        if (err) {
            console.error('Error al obtener el empleado:', err);
            return res.status(500).json({ error: 'Error al obtener el empleado' });
        }
        res.status(200).json(empleado);
    });
}

function registrarEmpleadoController(req, res) {
    const { nombre, cargo, salario, fecha_contratacion, fecha_terminacion, sueldo, planilla, tiempo_trabajo, asistencia_empleador, id_administrador } = req.body;

    registrarEmpleado(nombre, cargo, salario, fecha_contratacion, fecha_terminacion, sueldo, planilla, tiempo_trabajo, asistencia_empleador, id_administrador, (error, resultado) => {
        if (error) {
            console.error('Error al registrar el empleado:', error);
            return res.status(500).json({ error: 'Error al registrar el empleado' });
        }
        res.status(200).json(resultado);
    });
}

async function actualizarEmpleadoController(req, res) {
    const idEmpleado = req.params.id;
    const { nombre, cargo, salario, fecha_contratacion, fecha_terminacion, sueldo, planilla, tiempo_trabajo, asistencia_empleador, id_administrador } = req.body;

    try {
        // Llama a actualizarEmpleado utilizando async/await
        const resultados = await actualizarEmpleado(idEmpleado, nombre, cargo, salario, fecha_contratacion, fecha_terminacion, sueldo, planilla, tiempo_trabajo, asistencia_empleador, id_administrador);
        
        // Envía una respuesta con los resultados
        res.status(200).json({ message: 'Empleado actualizado exitosamente', results: resultados });
    } catch (error) {
        console.error('Error al actualizar el empleado:', error);
        res.status(500).json({ error: 'Error al actualizar el empleado' });
    }
}

function eliminarEmpleadoController(req, res) {
    const idEmpleado = req.params.id;

    eliminarEmpleado(idEmpleado, (error, resultado) => {
        if (error) {
            console.error('Error al eliminar el empleado:', error);
            return res.status(500).json({ error: 'Error al eliminar el empleado' });
        }
        res.status(200).json({ message: 'Empleado eliminado exitosamente', results: resultado });
    });
}

module.exports = { obtenerEmpleadoPorIDController, registrarEmpleadoController, actualizarEmpleadoController, eliminarEmpleadoController };

