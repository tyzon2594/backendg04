// services/elementosService.js

const { obtenerConexion } = require('../database/conexion_mysql');

function obtenerPersona(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }

    connection.query('SELECT * FROM persona', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
}

function modificarPersona(parametrosEntrada, callback) {
    obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
  
      const query = 'call ActualizarPersona(?,?,?,?,?,?,?,?,?,?, @parametro_salida)';
      connection.query(query, [parametrosEntrada.idPersona, parametrosEntrada.Nombre, parametrosEntrada.Apellidos, parametrosEntrada.TipoDocumento, parametrosEntrada.NumeroDocumento, parametrosEntrada.FechaRegistro, parametrosEntrada.Telefono, parametrosEntrada.Correo, parametrosEntrada.Activo, parametrosEntrada.Direccion], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
  
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
  
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }

  function insertarPersona(parametrosEntrada, callback) {
    obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
  
      const query = 'call InsertarPersona(?,?,?,?,?,?,?,?,?, @parametro_salida)';
      connection.query(query, [parametrosEntrada.Nombre, parametrosEntrada.Apellidos, parametrosEntrada.TipoDocumento, parametrosEntrada.NumeroDocumento, parametrosEntrada.FechaRegistro, parametrosEntrada.Telefono, parametrosEntrada.Correo, parametrosEntrada.Activo, parametrosEntrada.Direccion], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
  
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
  
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }

  
module.exports = { obtenerPersona,modificarPersona,insertarPersona };
