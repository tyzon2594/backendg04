// services/proveedorService.js

const mysql = require('mysql');

// Configurar la conexión a la base de datos
const connection = mysql.createConnection({
    host: 'localhost',  // Cambia esto por el host de tu base de datos
  user: 'root',    // Cambia esto por el nombre de usuario de tu base de datos
  password: '', // Cambia esto por la contraseña de tu base de datos
  database: 'bd_administrador' // Cambia esto por el nombre de tu base de datos
});

function obtenerProveedorPorID(idProveedor, callback) {
    connection.query('CALL ObtenerProveedorPorID(?)', [idProveedor], (error, results) => {
        if (error) {
            callback(error, null);
            return;
        }
        callback(null, results[0]);
    });
}

// Función para actualizar un proveedor
async function actualizarProveedor(idProveedor, nombreContactoNuevo, contactoTelefonoNuevo, contactoEmailNuevo, direccionNueva) {
    const nombreEmpresa = 'BEMBOS';
    const contactoNombre = nombreContactoNuevo || 'XAVIER';
    const contactoTelefono = contactoTelefonoNuevo || '987654321';
    const contactoEmail = contactoEmailNuevo || 'nuevo_correo@gmail.com';
    const direccion = direccionNueva || 'Otra Dirección';
    const idAdministrador = 1;

    try {
        const results = await new Promise((resolve, reject) => {
            connection.query('CALL ActualizarProveedor(?, ?, ?, ?, ?, ?, ?)', 
                             [idProveedor, nombreEmpresa, contactoNombre, contactoTelefono, contactoEmail, direccion, idAdministrador], 
                             (error, results) => {
                if (error) {
                    reject(error);
                    return;
                }
                resolve(results);
            });
        });
        return results;
    } catch (error) {
        throw error;
    }
}

function registrarProveedor(nombre_empresa, contacto_nombre, contacto_telefono, contacto_email, direccion, id_administrador, callback) {
    const query = 'CALL RegistrarProveedor(?, ?, ?, ?, ?, ?)';
    connection.query(query, [nombre_empresa, contacto_nombre, contacto_telefono, contacto_email, direccion, id_administrador], (error, results) => {
        if (error) {
            callback(error, null);
            return;
        }
        const nuevoProveedorId = results.insertId; // Aquí se obtiene el ID asignado por la base de datos al nuevo proveedor
        callback(null, { message: 'Proveedor registrado exitosamente', nuevoProveedorId });
    });
}
// Función para eliminar un proveedor
function eliminarProveedor(id_proveedor, callback) {
    const query = 'CALL EliminarProveedor(?)';
    connection.query(query, [id_proveedor], (error, results) => {
        if (error) {
            callback(error, null);
            return;
        }
        callback(null, results);
    });
}

module.exports = { obtenerProveedorPorID, actualizarProveedor, registrarProveedor, eliminarProveedor };