// services/elementosService.js

const { obtenerConexion } = require('../database/conexion_mysql');

function obtenerMesa(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }

    connection.query('CALL ObtenerMesas()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
}

function insertarMesa(parametrosEntrada, callback){
    obtenerConexion((err, connection) => {
        if (err) {
          return callback(err);
        }
    
        const query = 'CALL InsertarMesa(?, ?, ?,?,?,?, @parametro_salida)';
        connection.query(query, [parametrosEntrada.NombreMesa, parametrosEntrada.NumeroAsientos, parametrosEntrada.Activo, parametrosEntrada.FechaRegistro, parametrosEntrada.idPiso, parametrosEntrada.idEstado], (err) => {
          if (err) {
            connection.release();
            return callback(err);
          }
    
          connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
            connection.release();
            if (err) {
              return callback(err);
            }
    
            const parametroSalida = results[0].parametro_salida;
            callback(null, { parametroSalida });
          });
        });
      });
}

function modificarMesa(parametrosEntrada, callback) {
    obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
  
      const query = 'CALL ActualizarMesa(?, ?, ?,?,?,?,?, @parametro_salida)';
      connection.query(query, [parametrosEntrada.IdMesa, parametrosEntrada.NombreMesa, parametrosEntrada.NumeroAsientos, parametrosEntrada.Activo, parametrosEntrada.FechaRegistro, parametrosEntrada.IdPiso, parametrosEntrada.IdEstado], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
  
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
  
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }

module.exports = { obtenerMesa,modificarMesa, insertarMesa };