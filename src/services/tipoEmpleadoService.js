// services/elementosService.js

const { obtenerConexion } = require('../database/conexion_mysql');

function obtenerTipoEmpleado(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }

    connection.query('SELECT * FROM tipoempleado', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
}

function modificarTipoEmpleado(parametrosEntrada, callback) {
    obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
  
      const query = 'call ActualizarTipoEmpleado(?,?,?,?, @parametro_salida)';
      connection.query(query, [parametrosEntrada.idTipoEmpleado, parametrosEntrada.Descripcion, parametrosEntrada.Activo, parametrosEntrada.FechaRegistro], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
  
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
  
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }

  function insertarTipoEmpleado(parametrosEntrada, callback) {
    obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
  
      const query = 'call InsertarTipoEmpleado(?,?,?, @parametro_salida)';
      connection.query(query, [parametrosEntrada.Descripcion, parametrosEntrada.Activo, parametrosEntrada.FechaRegistro], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
  
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
  
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }

module.exports = { obtenerTipoEmpleado,modificarTipoEmpleado,insertarTipoEmpleado };
