// services/elementosService.js

const { obtenerConexion } = require('../database/conexion_mysql');

function obtenerPiso(callback) {
  obtenerConexion((err, connection) => {
    if (err) {
      return callback(err);
    }

    connection.query('CALL Obtener_Pisos()', (err, results) => {
      connection.release();
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  });
}

function insertarPiso(parametrosEntrada, callback){
    obtenerConexion((err, connection) => {
        if (err) {
          return callback(err);
        }
    
        const query = 'CALL InsertarPiso(?, ?, ?,?,?,?, @parametro_salida)';
        connection.query(query, [parametrosEntrada.NombrePiso, parametrosEntrada.Activo, parametrosEntrada.FechaRegistro, parametrosEntrada.Descripcion, parametrosEntrada.Capacidad, parametrosEntrada.idEstado], (err) => {
          if (err) {
            connection.release();
            return callback(err);
          }
    
          connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
            connection.release();
            if (err) {
              return callback(err);
            }
    
            const parametroSalida = results[0].parametro_salida;
            callback(null, { parametroSalida });
          });
        });
      });
}

function modificarPiso(parametrosEntrada, callback) {
    obtenerConexion((err, connection) => {
      if (err) {
        return callback(err);
      }
  
      const query = 'CALL ActualizarPiso(?, ?, ?,?,?,?,?, @parametro_salida)';
      connection.query(query, [parametrosEntrada.idPiso, parametrosEntrada.NombrePiso, parametrosEntrada.Activo, parametrosEntrada.FechaRegistro, parametrosEntrada.Descripcion, parametrosEntrada.Capacidad, parametrosEntrada.idEstado], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }
  
        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) {
            return callback(err);
          }
  
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
  }

module.exports = { obtenerPiso,modificarPiso, insertarPiso };
