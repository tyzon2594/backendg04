const { obtenerConexion } = require('../database/conexion_mysql');

function obtenerAsignacion(callback) {
  obtenerConexion((err, connection) => {
    if (err) { return callback(err); }

    connection.query('CALL Obtener_Asignaciones()', (err, results) => {
      connection.release();
      if (err) { return callback(err); }
      callback(null, results);
    });
  });
}

function insertarAsignacion(pe,callback){
  obtenerConexion((err, connection) =>{
    if(err){ return callback(err); }

    const query = 'CALL InsertarAsignacionMesa(?,?,?,?,?,?,?,@parametro_salida)';
    connection.query(query, [pe.IdEmpleado, pe.Fecha, pe.HoraInicio, pe.HoraFin, pe.idEstado, pe.IdCliente, pe.Observaciones], (err) => {
      if(err){ 
        connection.release(); 
        return callback(err);
      }

      connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
        connection.release();
        if (err) { return callback(err); }

        const parametroSalida = results[0].parametro_salida;
        callback(null, { parametroSalida });
      });
    });
  })
}

function actualizarAsignacion(pe, callback) {
    obtenerConexion((err, connection) => {
      if (err) { return callback(err); }
  
      const query = 'CALL ActualizarAsignacionMesa(?,?,?,?,?,?,?,?,@parametro_salida)';
      connection.query(query, [pe.IdAsignacion,pe.IdEmpleado, pe.Fecha, pe.HoraInicio, pe.HoraFin, pe.idEstado, pe.IdCliente, pe.Observaciones], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }

        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) { return callback(err); }
  
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
}

module.exports = { obtenerAsignacion, insertarAsignacion, actualizarAsignacion };