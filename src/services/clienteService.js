const { obtenerConexion } = require('../database/conexion_mysql');

function obtenerClientes(callback) {
  obtenerConexion((err, connection) => {
    if (err) { return callback(err); }

    connection.query('CALL Obtener_Clientes()', (err, results) => {
      connection.release();
      if (err) { return callback(err); }
      callback(null, results);
    });
  });
}

function insertarCliente(parametrosEntrada,callback){
  obtenerConexion((err, connection) =>{
    if(err){ return callback(err); }

    const query = 'CALL InsertarCliente(?,?,?,@parametro_salida)';
    connection.query(query, [parametrosEntrada.IdPersona, parametrosEntrada.FechaRegistro, parametrosEntrada.Activo], (err) => {
      if(err){ 
        connection.release(); 
        return callback(err);
      }

      connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
        connection.release();
        if (err) { return callback(err); }

        const parametroSalida = results[0].parametro_salida;
        callback(null, { parametroSalida });
      });
    });
  })
}

function actualizarCliente(parametrosEntrada, callback) {
    obtenerConexion((err, connection) => {
      if (err) { return callback(err); }
  
      const query = 'CALL ActualizarClientes(?, ?, ?,?, @parametro_salida)';
      connection.query(query, [parametrosEntrada.IdCliente, parametrosEntrada.IdPersona, parametrosEntrada.FechaRegistro, parametrosEntrada.Activo], (err) => {
        if (err) {
          connection.release();
          return callback(err);
        }

        connection.query('SELECT @parametro_salida AS parametro_salida', (err, results) => {
          connection.release();
          if (err) { return callback(err); }
  
          const parametroSalida = results[0].parametro_salida;
          callback(null, { parametroSalida });
        });
      });
    });
}

module.exports = { obtenerClientes, actualizarCliente,insertarCliente };
