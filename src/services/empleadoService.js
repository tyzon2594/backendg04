const mysql = require('mysql');

// Configurar la conexión a la base de datos
const connection = mysql.createConnection({
    host: 'localhost',      // Cambia esto por el host de tu base de datos
    user: 'root',           // Cambia esto por el nombre de usuario de tu base de datos
    password: '',           // Cambia esto por la contraseña de tu base de datos
    database: 'bd_administrador'  // Cambia esto por el nombre de tu base de datos
});

// Función para obtener un empleado por su ID
function obtenerEmpleadoPorID(idEmpleado, callback) {
    connection.query('CALL ObtenerEmpleadoPorID(?)', [idEmpleado], (error, results) => {
        if (error) {
            callback(error, null);
            return;
        }
        callback(null, results[0]);
    });
}

// Función para actualizar un empleado
async function actualizarEmpleado(idEmpleado, nombre, cargo, salario, fechaContratacion, fechaTerminacion, sueldo, planilla, tiempoTrabajo, asistenciaEmpleador, idAdministrador)
 {
    return new Promise((resolve, reject) => {
        const query = 'CALL ActualizarEmpleado(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        connection.query(query, [idEmpleado, nombre, cargo, salario, fechaContratacion, fechaTerminacion, sueldo, planilla, tiempoTrabajo, asistenciaEmpleador, idAdministrador], (error, results) => {
            if (error) {
                reject(error);
                return;
            }
            resolve(results);
        });
    });
}

// Función para registrar un nuevo empleado
function registrarEmpleado(nombre, cargo, salario, fechaContratacion, fechaTerminacion, sueldo, planilla, tiempoTrabajo, asistenciaEmpleador, idAdministrador, callback) {

    const query = 'CALL RegistrarEmpleado(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

    connection.query(query, [nombre, cargo, salario, fechaContratacion, fechaTerminacion, sueldo, planilla, tiempoTrabajo, asistenciaEmpleador, idAdministrador], (error, results) => {
        if (error) {
            callback(error, null);
            return;
        }
        const nuevoEmpleadoId = results.insertId; // Obtiene el ID asignado por la base de datos al nuevo empleado
        callback(null, { message: 'Empleado registrado exitosamente', nuevoEmpleadoId });
    });
}

// Función para eliminar un empleado
function eliminarEmpleado(idEmpleado, callback) {
    const query = 'CALL EliminarEmpleado(?)';
    connection.query(query, [idEmpleado], (error, results) => {
        if (error) {
            callback(error, null);
            return;
        }
        callback(null, results);
    });
}

module.exports = { obtenerEmpleadoPorID, actualizarEmpleado, registrarEmpleado, eliminarEmpleado };
