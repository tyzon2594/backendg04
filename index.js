<<<<<<< HEAD
const express = require('express');
const { obtenerEmpleadoPorIDController, registrarEmpleadoController, actualizarEmpleadoController, eliminarEmpleadoController } = require('./src/controllers/empleadoController');

//tabla cliente
const {obtenerClientesController,actualizarClienteController, insertarClienteController } = require('./src/controllers/clienteController');

//Tabla AsignacionMesas
<<<<<<< HEAD
const { obtenerAsignacionController, actualizarAsignacionController, insertarAsignacionMesaController } = require('./src/controllers/asignacionMesaController');

// Tabla TipoEmpleado
const { obtenerTipoEmpleadoController, modificarTipoEmpleadoController, insertarTipoEmpleadoController } = require('./src/controllers/tipoEmpleadoController');

// Tabla Persona
const { obtenerPersonaController, modificarPersonaController, insertarPersonaController } = require('./src/controllers/personaController');
<<<<<<< HEAD

// tabla piso
const { obtenerPisosController, modificarPisoController, insertarPisoController } = require('./src/controllers/pisoController');
// tabla mesas
const { obtenerMesasController, modificarMesaController, insertarMesaController } = require('./src/controllers/mesaController');

>>>>>>> cb97bde514cda97588e0f066df5770f06a70057d
=======

// index.js

const express = require('express');
const { obtenerProveedorPorIDController, registrarProveedorController, actualizarProveedorController, eliminarProveedorController } = require('./src/controllers/proveedorController');
>>>>>>> fae878916f9fc937de93048c6ace50994eca12f7

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json());

<<<<<<< HEAD
// Ruta para obtener un empleado por su ID
app.get('/empleado/:id', obtenerEmpleadoPorIDController);

// Ruta para registrar un nuevo empleado
app.post('/empleado', registrarEmpleadoController);

// Ruta para actualizar un empleado por su ID
app.put('/empleado/:id', actualizarEmpleadoController);

// Ruta para eliminar un empleado por su ID
app.delete('/empleado/:id', eliminarEmpleadoController);

//tabla cliente
app.get('/obtenerClientes', obtenerClientesController);
app.post('/actualizarClientes',actualizarClienteController);
app.post('/insertarCliente',insertarClienteController);
//Tabla asignacion
app.get('/obtenerAsignacion', obtenerAsignacionController);
app.post('/actualizarAsignacion',actualizarAsignacionController);
app.post('/insertarAsignacion',insertarAsignacionMesaController);
// Tabla TipoEmpleado
app.get('/obtenerTipoEmpleado', obtenerTipoEmpleadoController);
app.post('/modificarTipoEmpleado', modificarTipoEmpleadoController);
app.put('/insertarTipoEmpleado', insertarTipoEmpleadoController);
// Tabla Persona
app.get('/obtenerPersona', obtenerPersonaController);
app.post('/modificarPersona', modificarPersonaController);
app.put('/insertarPersona', insertarPersonaController);

//tabla mesas
app.get('/obtenerMesas', obtenerMesasController);
app.post('/modificarMesa', modificarMesaController);
app.post('/insertarMesa',insertarMesaController);

//tabla pisos
app.get('/obtenerPisos', obtenerPisosController);
app.post('/modificarPiso', modificarPisoController);
app.post('/insertarPiso',insertarPisoController);

=======
// Ruta para obtener un proveedor por su ID
app.get('/proveedor/:id', obtenerProveedorPorIDController);

// Ruta para registrar un nuevo proveedor
app.post('/proveedor', registrarProveedorController);

// Ruta para actualizar un proveedor por su ID
app.put('/proveedor/:id', actualizarProveedorController);


// Ruta para eliminar un proveedor por su ID
app.delete('/proveedor/:id', eliminarProveedorController);


>>>>>>> fae878916f9fc937de93048c6ace50994eca12f7



app.listen(PORT, () => {
    console.log(`Servidor API REST escuchando en el puerto ${PORT}`);
});
